<?php
namespace App\Helpers\Teleopti;

use Exception;

abstract class AbstractScaper {
	protected $curl_handles = [];
	
	abstract protected function getLoginParameters($username, $password);
	
	protected function getLoginUrl() {
		return static::URL_BASE . static::URL_LOGIN;
	}
	
	protected function getScheduleUrl() {
		return static::URL_BASE . static::URL_SCHEDULE;
	}
	
	protected static function createCurlHandle() {
		$ch = curl_init();
		
		if ($ch === false) {
			throw new Exception('no cURL?!');
		}
		
		if (curl_setopt_array($ch, [
			CURLOPT_COOKIEFILE => '',
			CURLOPT_RETURNTRANSFER => true,
		]) === false) {
			curl_close($ch);
			throw new Exception('cURL setopt error');
		}
				
		return $ch;
	}
	
	public function loginPassword($username, $password) {
		if (array_key_exists($username, $this->curl_handles)) {
			return $this->curl_handles[$username];
		}
		
		$ch = static::createCurlHandle();
		
		if (curl_setopt_array($ch, [
			CURLOPT_URL => static::getLoginUrl(),
			CURLOPT_POSTFIELDS => $this->getLoginParameters($username, $password),
		]) === false) {
			curl_close($ch);
			throw new Exception('cURL setopt error');
		}
		
		$result = curl_exec($ch);
		
		if ($result === false) {
			curl_close($ch);
			throw new Exception('cURL exec error');
		}
		
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		
		if ($code !== 200) {
			curl_close($ch);
			throw new Exception('Login failed: ' . $result);
		}

		$this->curl_handles[$username] = $ch;
	}
	
	public function loginCookie($username, $cookie) {
		if (array_key_exists($username, $this->curl_handles)) {
			return $this->curl_handles[$username];
		}
		
		$ch = static::createCurlHandle();
		
		if (curl_setopt_array($ch, [
			CURLOPT_COOKIE => $cookie,
		]) === false) {
			curl_close($ch);
			throw new Exception('cURL setopt error');
		}
		
		$this->curl_handles[$username] = $ch;
	}
	
	public function getSchedule($username, $day = null) {
		if (!array_key_exists($username, $this->curl_handles)) {
			throw new Exception('No login handle found for user ' . $username);
		}
		
		$ch = $this->curl_handles[$username];
		$url = static::getScheduleUrl();
		
		if (!is_null($day)) {
			$url .= '?' . http_build_query([
				'date' => $day,
			]);
		}
		
		if (curl_setopt_array($ch, [
			CURLOPT_HTTPGET => true,
			CURLOPT_URL => $url,
		]) === false) {
			curl_close($ch);
			throw new Exception('cURL setopt error');
		}
		
		$result = curl_exec($ch);

		if ($result === false) {
			curl_close($ch);
			throw new Exception('cURL exec error');
		}
		
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		
		if ($code !== 200) {
			curl_close($ch);
			throw new Exception('Scraping failed: ' . $result);
		}
		
		$json = json_decode($result, true);
		
		if ($json === false) {
			throw new Exception('JSON decode failed');
		}
		
		return $json;
	}
	
	function logout($username) {
		if (!array_key_exists($username, $this->curl_handles)) {
			return true;
		}
		
		curl_close($this->curl_handles[$username]);
		unset($this->curl_handles[$username]);
	}
}
