<?php
namespace App\Helpers\Teleopti;

use Exception;

class ScraperV8 extends AbstractScaper {
	const URL_BASE = 'https://mytime.vivacom.bg/TeleoptiWFM/Web/';
	const URL_LOGIN = 'SSO/ApplicationAuthenticationApi/Password';
	const URL_SCHEDULE = 'api/Schedule/FetchData';
	
	protected function getLoginParameters($username, $password) {
		return [
			'type' => 'application',
			'username' => $username,
			'password' => $password,
			'rememberMe' => 'false',
			'IsLogonFromBrowser' => 'true',
		];
	}
}
