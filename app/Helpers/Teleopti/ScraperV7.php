<?php
namespace App\Helpers\Teleopti;

use Exception;

class ScraperV7 extends AbstractScaper {
	const URL_BASE = 'https://mytime.vivacom.bg/TeleoptiCCC/Web/';
	const URL_LOGIN = 'Start/AuthenticationApi/Logon';
	const URL_SCHEDULE = 'MyTime/Schedule/FetchData';
	
	protected function getLoginParameters($username, $password) {
		return [
			'isWindows' => 'false',
			'type' => 'application',
			'datasource' => 'Teleopti CCC',
			'businessUnitId' => '144fff42-10d2-4589-8466-9dd100cd8d77',
			'username' => $username,
			'password' => $password,
		];
	}
}
