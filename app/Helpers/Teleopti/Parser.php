<?php
namespace App\Helpers\Teleopti;

use Carbon\Carbon;

class Parser {
	private static function parseTimespan($date, $text, $first = false) {
		$plusone = 0;
		
		if (strpos($text, ' +1') === strlen($text) - 3) {
			$plusone = 1;
			$text = substr($text, 0, -3);
		}
		
		if ($first && $plusone) {
			return false;
		}
		
		$parts = explode(' - ', $text);
		
		if (count($parts) !== 2) {
			return false;
		}

		$local = 'Europe/Sofia';
		$utc = 'UTC';
		
		$start = Carbon::parse($date . ' ' . $parts[0], $local)->setTimezone($utc);
		$end = Carbon::parse($date . ' ' . $parts[1], $local)->addDays($plusone)->setTimezone($utc);

		return compact('start', 'end');
	}

	public static function parseSchedule($json) {
		$periods = [];

		foreach ($json['Days'] as $day) {
			foreach ($day['Periods'] as $idx => $period) {
				$timespan = static::parseTimespan($day['FixedDate'], $period['TimeSpan'], $idx === 0);
				
				if ($timespan === false) {
					continue;
				}

				$periods[] = [
					'shift_title' => $day['Summary']['Title'],
					'shift_duration' => $day['Summary']['Summary'],
					'type' => $period['Title'],
					'start' => $timespan['start'],
					'end' => $timespan['end'],
				];
			}
		}
		
		return $periods;
	}
}
