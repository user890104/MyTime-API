<?php
namespace App\Http\Controllers;

use App\Helpers\Teleopti\Parser;
use App\Period;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
	public function view() {
		$all_schedules = Period::orderBy('start')
			->get(['username', 'type', 'start', 'end', 'shift_title', 'shift_duration']);
		
		$schedules = [];
		
		foreach ($all_schedules as $schedule) {
			$username = $schedule->username;
			unset($schedule->username);
			
			if (!array_key_exists($username, $schedules)) {
				$schedules[$username] = [];
			}
			
			$schedules[$username][] = $schedule;
		}
		
		return response()->json($schedules);
	}
	
	public function update(Request $request) {
		$hasVersion = $request->has('version');
		$hasUser = $request->has('username');
		$hasPass = $request->has('password');
		$hasCookie = $request->has('cookie');
		
		if (!$hasVersion || !$hasUser || !($hasPass || $hasCookie)) {
			return response()->json([
				'error' => 'missing params',
			], 422);
		}
		switch ($request->input('version')) {
			case 7:
				$scraper = new \App\Helpers\Teleopti\ScraperV7;
				break;
			case 8:
				$scraper = new \App\Helpers\Teleopti\ScraperV8;
				break;
			default:
				return response()->json([
					'error' => 'unsupported version',
				], 422);
				break;
		}

		$username = $request->input('username');

		if ($hasPass) {
			$scraper->loginPassword($username, $request->input('password'));
		}
		elseif ($hasCookie) {
			$scraper->loginCookie($username, $request->input('cookie'));
		}
		else {
			throw new Exception('No supported authentication method available');
		}
		
		$schedule = [];
		$date = new Carbon('last sunday');
		
		do {
			$json = $scraper->getSchedule($username, $date->format('Y/m/d'));
			$currSchedule = Parser::parseSchedule($json);
			$schedule = array_merge($schedule, $currSchedule);
			$date->addWeek();
		}
		while (count($currSchedule) > 0);
		
		$scraper->logout($username);
		
		Period::where('username', $username)->delete();
		
		array_walk($schedule, function(&$value) use ($username) {
			$value['username'] = $username;
		});
		
		Period::insert($schedule);

		return response()->json([
			'status' => 'OK',
			'entries' => count($schedule),
		]);
	}
}
