<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DateTime;

class Period extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'start', 'end', 'type', 'shift_title', 'shift_duration',
    ];
	
	protected $dates = [
		'start', 'end',
	];
	
    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTime  $date
     * @return string
     */
    protected function serializeDate(DateTime $date)
    {
        return intval($date->format('U'));
    }
}
